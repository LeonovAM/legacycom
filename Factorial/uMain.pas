unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    btnShow: TButton;
    procedure btnShowClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses uTestObject;

{$R *.dfm}

procedure TForm1.btnShowClick(Sender: TObject);
const
  FactorialFor = 4;
var
  Obj: TTestObject;
  F: Integer;
begin
  Obj := TTestObject.Create;
  try
    F := Obj.CalcFactorial(FactorialFor);
    ShowMessage('Factorial is ' + IntToStr(F));
  finally
    Obj.Free;
  end;
end;

end.
