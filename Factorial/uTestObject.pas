unit uTestObject;

interface

type
  TTestObject = class
  public
    function CalcFactorial(Number: Integer): Integer;
  end;

implementation

{ TTestObject }

function TTestObject.CalcFactorial(Number: Integer): Integer;
begin
  Result := 1;
  if Number = 0 then
    Exit;

  Result := Number * CalcFactorial(Number - 1);
end;

end.
