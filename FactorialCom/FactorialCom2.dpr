library FactorialCom2;

uses
  ComServ,
  FactorialCom2_TLB in 'FactorialCom2_TLB.pas',
  uFactorial2 in 'uFactorial2.pas' {Factorial2: CoClass},
  uTestObject in '..\Factorial\uTestObject.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
