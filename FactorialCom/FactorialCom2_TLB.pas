unit FactorialCom2_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 28.05.2024 6:11:01 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Projects\Delphi\LegacyCom\FactorialCom\FactorialCom2.tlb (1)
// LIBID: {801CF371-661A-45D2-B16C-81695FC1F307}
// LCID: 0
// Helpfile: 
// HelpString: FactorialCom2 Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  FactorialCom2MajorVersion = 1;
  FactorialCom2MinorVersion = 0;

  LIBID_FactorialCom2: TGUID = '{801CF371-661A-45D2-B16C-81695FC1F307}';

  IID_IFactorial2: TGUID = '{15B0BEC5-8BC9-495A-B801-EC1B632533A6}';
  CLASS_Factorial2: TGUID = '{7CFE08B5-8B8E-4A88-8C6D-48E9C8CF788F}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IFactorial2 = interface;
  IFactorial2Disp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Factorial2 = IFactorial2;


// *********************************************************************//
// Interface: IFactorial2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {15B0BEC5-8BC9-495A-B801-EC1B632533A6}
// *********************************************************************//
  IFactorial2 = interface(IDispatch)
    ['{15B0BEC5-8BC9-495A-B801-EC1B632533A6}']
    function CalcFactorial(Number: SYSINT): SYSINT; safecall;
  end;

// *********************************************************************//
// DispIntf:  IFactorial2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {15B0BEC5-8BC9-495A-B801-EC1B632533A6}
// *********************************************************************//
  IFactorial2Disp = dispinterface
    ['{15B0BEC5-8BC9-495A-B801-EC1B632533A6}']
    function CalcFactorial(Number: SYSINT): SYSINT; dispid 201;
  end;

// *********************************************************************//
// The Class CoFactorial2 provides a Create and CreateRemote method to          
// create instances of the default interface IFactorial2 exposed by              
// the CoClass Factorial2. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoFactorial2 = class
    class function Create: IFactorial2;
    class function CreateRemote(const MachineName: string): IFactorial2;
  end;

implementation

uses ComObj;

class function CoFactorial2.Create: IFactorial2;
begin
  Result := CreateComObject(CLASS_Factorial2) as IFactorial2;
end;

class function CoFactorial2.CreateRemote(const MachineName: string): IFactorial2;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Factorial2) as IFactorial2;
end;

end.
