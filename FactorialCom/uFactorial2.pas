unit uFactorial2;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, ActiveX, FactorialCom2_TLB, StdVcl;

type
  TFactorial2 = class(TAutoObject, IFactorial2)
  protected
    function CalcFactorial(Number: SYSINT): SYSINT; safecall;
                              
  end;

implementation

uses ComServ, uTestObject;

function TFactorial2.CalcFactorial(Number: SYSINT): SYSINT;
var
  Obj: TTestObject;
begin
  Obj := TTestObject.Create;
  try
    Result := Obj.CalcFactorial(Number);
  finally
    Obj.Free;
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TFactorial2, Class_Factorial2,
    ciMultiInstance, tmApartment);
end.
