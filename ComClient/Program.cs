﻿using Interop.FactorialCom2;

namespace ComClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            Factorial2 factor = new();
            int findFor = 4;
            int result = factor.CalcFactorial(findFor);

            Console.WriteLine($"Factorial of {findFor} is {result}");
            Console.ReadLine();
        }
    }
}
